<div align="center">
  <h1><code>players-stats</code></h1>

  <strong>🏈 NFL players stats app</strong>
  <br>
  <a href="https://gitlab.com/viniarck/players-stats/-/commits/develop"><img alt="pipeline status" src="https://gitlab.com/viniarck/players-stats/badges/develop/pipeline.svg" /></a>
</div>

## Docs

- This web app was built based on these [requirements](./docs/requirements.md).
- You can find more details about the implementation, rationale and architecture on [docs/README.md](./docs/README.md).

## Development

To facilitate development, there's a [Makefile](./Makefile) with some frequently used commands.

#### Pre-requisites

Make sure you have installed these dependencies:

- `docker`
- `docker-compose`
- `make`

### How to run

- `make compose-up`

The `backend` container when starting up will try to auto load some [rushing stats entries](./backend/priv/repo/rushing.json) in the database, if you need to re-run, `make run-seeds`, notice this command is idempotent.

These three containers `db`, `backend` and `frontend` are expected to spun up:

```
docker ps

CONTAINER ID   IMAGE                    COMMAND                  CREATED          STATUS          PORTS                                       NAMES
d8813970fa2e   players-stats_frontend   "docker-entrypoint.s…"   50 seconds ago   Up 10 seconds   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp   frontend
4fe73f23872e   players-stats_backend    "bash -c /app/presta…"   52 seconds ago   Up 50 seconds   0.0.0.0:4000->4000/tcp, :::4000->4000/tcp   backend
8643c71ea804   postgres:13.0-alpine     "docker-entrypoint.s…"   53 seconds ago   Up 52 seconds   0.0.0.0:5432->5432/tcp, :::5432->5432/tcp   db
```


#### Trying it out

Once the `db` and `backend` have completely started and loaded the player stats entries,
locally with docker running, if you open [this http://localhost:8080 page](http://localhost:8080) that is served by the `frontend` dev cointainer, you should see a similar web page like this one below:

![feimg](./docs/fe-image.png)

If you need to further inspect or debug the output of the containers, try running `docker logs <container_name>`, and then if you need to take any action at all, most likely you'll have a `make` command target in the [Makefile](./Makefile) that would have what you're probably looking for.

### How to build

If you need to force a build from strach, and get it running again:

- `make build`

### How to test

Once you have it running:

- `make test`

### How to stop

- `make compose-down`
