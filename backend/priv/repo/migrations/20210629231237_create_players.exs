defmodule App.Repo.Migrations.CreatePlayers do
  use Ecto.Migration

  def change do
    create table(:players, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:name, :text, null: false)
      add(:nfl_team, :text)
      add(:pos, :text)
      add(:deleted_at, :naive_datetime)

      timestamps()
    end
  end
end
