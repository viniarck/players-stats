defmodule App.Repo.Migrations.PlayersNameUnique do
  use Ecto.Migration

  def change do
    drop_if_exists(index("players", [:name]))
    create(unique_index(:players, [:name]))
  end
end
