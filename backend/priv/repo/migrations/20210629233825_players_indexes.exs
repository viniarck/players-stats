defmodule App.Repo.Migrations.PlayersIndexes do
  use Ecto.Migration

  def change do
    create(index(:players, [:name]))
    create(index(:players, [:pos]))
    create(index(:players, [:nfl_team]))
    create(index(:players, [:deleted_at]))
  end
end
