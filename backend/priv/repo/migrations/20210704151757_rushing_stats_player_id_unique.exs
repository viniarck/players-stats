defmodule App.Repo.Migrations.RushingStatsPlayerIdUnique do
  use Ecto.Migration

  def change do
    drop_if_exists(index(:rushing_stats, [:player_id]))
    create(unique_index(:rushing_stats, [:player_id]))
  end
end
