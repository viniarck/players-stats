defmodule App.Repo.Migrations.CreateRushingStats do
  use Ecto.Migration

  def change do
    create table(:rushing_stats, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:att_g, :float)
      add(:att, :integer)
      add(:yds, :integer)
      add(:avg, :float)
      add(:yds_g, :float)
      add(:td, :integer)
      add(:lng, :integer)
      add(:lng_td, :boolean, default: false, null: false)
      add(:first_downs, :float)
      add(:first_down_percentage, :float)
      add(:twenty_plus_downs, :integer)
      add(:forty_plus_downs, :integer)
      add(:fum, :integer)
      add(:deleted_at, :naive_datetime)
      add(:player_id, references(:players, on_delete: :nothing, type: :binary_id))

      timestamps()
    end

    create(index(:rushing_stats, [:player_id]))
    create(index(:rushing_stats, [:deleted_at]))
  end
end
