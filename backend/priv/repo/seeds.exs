# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     App.Repo.insert!(%App.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

defmodule App.ParseRushing do
  alias App.RushingStats
  alias App.Players

  def read_file(file_name \\ "rushing.json") do
    File.read!(
      Path.dirname(__ENV__.file)
      |> Path.join(file_name)
    )
    |> Jason.decode!()
  end

  defp build_rushing(entry, player_id) do
    %{
      att: Map.get(entry, "Att"),
      att_g: Map.get(entry, "Att/G"),
      avg: Map.get(entry, "Avg"),
      first_down_percentage: Map.get(entry, "1st%", 0.0),
      first_downs: Map.get(entry, "1st", 0),
      forty_plus_downs: Map.get(entry, "40+", 0),
      fum: Map.get(entry, "FUM", 0),
      lng:
        Map.get(entry, "Lng", "0")
        |> to_string
        |> String.replace_suffix("T", "")
        |> String.to_integer(),
      lng_td: Map.get(entry, "Lng", "") |> to_string |> String.ends_with?("T"),
      td: Map.get(entry, "TD", 0),
      twenty_plus_downs: Map.get(entry, "20+", 0),
      yds:
        Map.get(entry, "Yds", "0") |> to_string |> String.replace(",", "") |> String.to_integer(),
      yds_g: Map.get(entry, "Yds/G", 0.0),
      player_id: player_id
    }
  end

  def insert_rows(lines) do
    lines
    |> Enum.each(fn entry ->
      player_attrs = %{
        name: Map.get(entry, "Player", ""),
        pos: Map.get(entry, "Pos", ""),
        nfl_team: Map.get(entry, "Team", "")
      }

      case Players.get_player_by_name(player_attrs[:name]) do
        nil ->
          with {:ok, player} <- Players.create_player(player_attrs),
               rushing_attrs <- build_rushing(entry, player.id) do
            {:ok, _rushing} = RushingStats.create_rushing_stat(rushing_attrs)
          else
            {:error, e} -> IO.inspect(e)
          end

        _ ->
          nil
      end
    end)
  end
end

App.ParseRushing.read_file() |> App.ParseRushing.insert_rows()
