defmodule AppWeb.RushingStatControllerTest do
  use AppWeb.ConnCase

  alias App.RushingStats
  alias App.Players
  alias App.RushingStats.RushingStat

  @create_attrs %{
    att: 42,
    att_g: 120.5,
    avg: 120.5,
    first_down_percentage: 120.5,
    first_downs: 120.5,
    forty_plus_downs: 42,
    fum: 42,
    lng: 42,
    lng_td: true,
    td: 42,
    twenty_plus_downs: 42,
    yds: 42,
    yds_g: 120.5
  }
  @player_attrs %{
    name: "some player",
    nfl_team: "some team",
    pos: "QB"
  }
  @update_attrs %{}
  @invalid_attrs %{att: "invalid"}

  def fixture(attrs \\ %{}) do
    {:ok, rushing_stat} =
      attrs
      |> Enum.into(@create_attrs)
      |> RushingStats.create_rushing_stat()

    rushing_stat
  end

  def player_fixture(attrs \\ %{}) do
    {:ok, player} =
      attrs
      |> Enum.into(@player_attrs)
      |> Players.create_player()

    player
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all rushing_stats and their attributes", %{conn: conn} do
      player = player_fixture()
      rushing = fixture(%{player_id: player.id})
      conn = get(conn, Routes.rushing_stat_path(conn, :index))
      res = json_response(conn, 200)
      assert res["pagination"]["total"] == 1
      assert length(res["data"]) == 1

      rushing_data = res["data"] |> hd
      assert rushing_data["id"] == rushing.id
      assert rushing_data["att"] == rushing.att
      assert rushing_data["att_g"] == rushing.att_g
      assert rushing_data["avg"] == rushing.avg
      assert rushing_data["first_down_percentage"] == rushing.first_down_percentage
      assert rushing_data["first_downs"] == rushing.first_downs
      assert rushing_data["forty_plus_downs"] == rushing.forty_plus_downs
      assert rushing_data["fum"] == rushing.fum
      assert rushing_data["lng"] == rushing.lng
      assert rushing_data["lng_td"] == rushing.lng_td
      assert rushing_data["td"] == rushing.td
      assert rushing_data["twenty_plus_downs"] == rushing.twenty_plus_downs
      assert rushing_data["yds"] == rushing.yds
      assert rushing_data["yds_g"] == rushing.yds_g
      assert rushing_data["player"]["name"] == player.name
      assert rushing_data["player"]["nfl_team"] == player.nfl_team
      assert rushing_data["player"]["pos"] == player.pos
    end

    test "lists all rushing_stats as csv", %{conn: conn} do
      fixture()
      conn = get(conn, Routes.rushing_stat_path(conn, :index, %{"as_csv" => true}))
      res = json_response(conn, 200)
      assert String.contains?(res["data"], ",")
      assert res["pagination"]["total"] == 1
    end

    test "test list rushing_stats client error changeset", %{conn: conn} do
      fixture()
      conn = get(conn, Routes.rushing_stat_path(conn, :index, %{"page" => 0}))
      res = json_response(conn, 400)
      assert String.contains?(res["errors"] |> hd |> Map.get("key"), "page")
    end
  end

  describe "create rushing_stat" do
    test "renders rushing_stat when data is valid", %{conn: conn} do
      conn = post(conn, Routes.rushing_stat_path(conn, :create), rushing_stat: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.rushing_stat_path(conn, :show, id))

      assert %{
               "id" => _id
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.rushing_stat_path(conn, :create), rushing_stat: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update rushing_stat" do
    setup [:create_rushing_stat]

    test "renders rushing_stat when data is valid", %{
      conn: conn,
      rushing_stat: %RushingStat{id: id} = rushing_stat
    } do
      conn =
        put(conn, Routes.rushing_stat_path(conn, :update, rushing_stat),
          rushing_stat: @update_attrs
        )

      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.rushing_stat_path(conn, :show, id))

      assert %{
               "id" => _id
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, rushing_stat: rushing_stat} do
      conn =
        put(conn, Routes.rushing_stat_path(conn, :update, rushing_stat),
          rushing_stat: @invalid_attrs
        )

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete rushing_stat" do
    setup [:create_rushing_stat]

    test "deletes chosen rushing_stat", %{conn: conn, rushing_stat: rushing_stat} do
      conn = delete(conn, Routes.rushing_stat_path(conn, :delete, rushing_stat))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.rushing_stat_path(conn, :show, rushing_stat))
      end
    end
  end

  defp create_rushing_stat(_) do
    rushing_stat = fixture()
    %{rushing_stat: rushing_stat}
  end
end
