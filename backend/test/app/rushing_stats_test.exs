defmodule App.RushingStatsTest do
  use App.DataCase

  alias App.RushingStats
  alias App.Players

  describe "rushing_stats" do
    alias App.RushingStats.RushingStat

    @valid_attrs %{
      att: 42,
      att_g: 120.5,
      avg: 120.5,
      first_down_percentage: 120.5,
      first_downs: 120.5,
      forty_plus_downs: 42,
      fum: 42,
      lng: 42,
      lng_td: true,
      td: 42,
      twenty_plus_downs: 42,
      yds: 42,
      yds_g: 120.5
    }
    @update_attrs %{
      att: 43,
      att_g: 456.7,
      avg: 456.7,
      deleted_at: ~N[2011-05-18 15:01:01],
      first_down_percentage: 456.7,
      first_downs: 456.7,
      forty_plus_downs: 43,
      fum: 43,
      lng: 43,
      lng_td: false,
      td: 43,
      twenty_plus_downs: 43,
      yds: 43,
      yds_g: 456.7
    }
    @invalid_attrs %{
      att: nil,
      att_g: nil,
      avg: nil,
      first_down_percentage: nil,
      first_downs: nil,
      forty_plus_downs: nil,
      fum: nil,
      lng: nil,
      lng_td: nil,
      td: nil,
      twenty_plus_downs: nil,
      yds: nil,
      yds_g: nil
    }

    def rushing_stat_fixture(attrs \\ %{}) do
      {:ok, rushing_stat} =
        attrs
        |> Enum.into(@valid_attrs)
        |> RushingStats.create_rushing_stat()

      rushing_stat
    end

    def player_fixture(attrs \\ %{}) do
      {:ok, player} =
        attrs
        |> Enum.into(%{name: "some player"})
        |> Players.create_player()

      player
    end

    test "list_rushing_stats/1 returns all rushing_stats" do
      rushing_stat_fixture()
      assert length(RushingStats.list_rushing_stats()) == 1
    end

    test "list_rushing_stats(per_page: 50, page:1)/1 returns all rushing_stats with count" do
      0..2 |> Enum.each(fn _ -> rushing_stat_fixture() end)
      {_, 3} = RushingStats.list_rushing_stats(per_page: 50, page: 1)
    end

    test "list_rushing_stats/1 limit and offset" do
      1..3 |> Enum.each(fn x -> rushing_stat_fixture(%{att: x}) end)

      1..3
      |> Enum.each(fn page ->
        {stats, 3} =
          RushingStats.list_rushing_stats(
            count: true,
            page: page,
            per_page: 1,
            order_by: [desc: :att]
          )

        assert stats |> hd |> Map.get(:att) == 4 - page
      end)
    end

    test "list_rushing_stats/1 player name case insensitive filter" do
      player = player_fixture(%{name: "Some Name"})
      {:ok, _} = RushingStats.create_rushing_stat(%{att: 10, player_id: player.id})
      assert RushingStats.list_rushing_stats(filters: %{name: "inexistent"}) == []

      assert RushingStats.list_rushing_stats(filters: %{name: player.name})
             |> hd
             |> Map.get(:player)
             |> Map.get(:name) == player.name

      assert RushingStats.list_rushing_stats(filters: %{name: "some n"})
             |> hd
             |> Map.get(:player)
             |> Map.get(:name) == player.name
    end

    test "list_rushing_stats/1 order_by desc :td" do
      {highest_td, lowest_td} = {60, 10}
      {:ok, _} = RushingStats.create_rushing_stat(%{td: highest_td})
      {:ok, _} = RushingStats.create_rushing_stat(%{td: lowest_td})

      assert RushingStats.list_rushing_stats(order_by: [desc: :td]) |> Enum.map(fn x -> x.td end) ==
               [highest_td, lowest_td]

      assert RushingStats.list_rushing_stats(order_by: [asc: :td]) |> Enum.map(fn x -> x.td end) ==
               [lowest_td, highest_td]
    end

    test "list_rushing_stats/1 order_by player attrs" do
      {player_1_name, player_2_name} = {"player_1", "player_2"}
      player_1 = player_fixture(%{name: player_1_name})
      player_2 = player_fixture(%{name: player_2_name})
      rushing_stat_fixture(%{td: 10, player_id: player_1.id})
      rushing_stat_fixture(%{td: 20, player_id: player_2.id})

      rushing_stats = RushingStats.list_rushing_stats(order_by: [asc: :name])

      assert rushing_stats |> Enum.map(fn x -> x.player.name end) == [
               player_1_name,
               player_2_name
             ]

      rushing_stats = RushingStats.list_rushing_stats(order_by: [desc: :name])

      assert rushing_stats |> Enum.map(fn x -> x.player.name end) == [
               player_2_name,
               player_1_name
             ]
    end

    test "list_rushing_stats/1 multiple order_by desc :td and :lng" do
      players_td_lng = [{60, 10}, {40, 20}, {50, 20}]

      players_td_lng
      |> Enum.each(fn values ->
        {:ok, _} = RushingStats.create_rushing_stat(%{td: elem(values, 0), lng: elem(values, 1)})
      end)

      rushing_stats = RushingStats.list_rushing_stats(order_by: [desc: :lng, desc: :td])

      assert rushing_stats |> Enum.map(fn x -> {x.td, x.lng} end) == [
               {50, 20},
               {40, 20},
               {60, 10}
             ]
    end

    test "list_rushing_stats/1 multiple order_by :lng with lng_td tiebreaker" do
      players_lng_and_lng_td = [{30, false}, {30, true}]

      players_lng_and_lng_td
      |> Enum.each(fn values ->
        {:ok, _} =
          RushingStats.create_rushing_stat(%{lng: elem(values, 0), lng_td: elem(values, 1)})
      end)

      players = RushingStats.list_rushing_stats(order_by: [desc: :lng, desc: :lng_td])
      assert players |> Enum.map(fn x -> {x.lng, x.lng_td} end) == [{30, true}, {30, false}]
    end

    test "get_rushing_stat!/1 returns the rushing_stat with given id" do
      rushing_stat = rushing_stat_fixture()
      assert RushingStats.get_rushing_stat!(rushing_stat.id).id == rushing_stat.id
    end

    test "get_rushing_stat!/1 returns the rushing_stat with association" do
      player = player_fixture()
      att = 10
      {:ok, rushing_stat} = RushingStats.create_rushing_stat(%{att: att, player_id: player.id})
      rushing_stat = RushingStats.get_rushing_stat!(rushing_stat.id)
      assert {rushing_stat.att, rushing_stat.player.name} == {att, player.name}
    end

    test "create_rushing_stat/1 with valid data creates a rushing_stat" do
      assert {:ok, %RushingStat{} = rushing_stat} = RushingStats.create_rushing_stat(@valid_attrs)
      assert rushing_stat.att == 42
      assert rushing_stat.att_g == 120.5
      assert rushing_stat.avg == 120.5
      assert is_nil(rushing_stat.deleted_at)
      assert rushing_stat.first_down_percentage == 120.5
      assert rushing_stat.first_downs == 120.5
      assert rushing_stat.forty_plus_downs == 42
      assert rushing_stat.fum == 42
      assert rushing_stat.lng == 42
      assert rushing_stat.lng_td == true
      assert rushing_stat.td == 42
      assert rushing_stat.twenty_plus_downs == 42
      assert rushing_stat.yds == 42
      assert rushing_stat.yds_g == 120.5
    end

    test "create_rushing_stat/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = RushingStats.create_rushing_stat(@invalid_attrs)
    end

    test "update_rushing_stat/2 with valid data updates the rushing_stat" do
      rushing_stat = rushing_stat_fixture()

      assert {:ok, %RushingStat{} = rushing_stat} =
               RushingStats.update_rushing_stat(rushing_stat, @update_attrs)

      assert rushing_stat.att == 43
      assert rushing_stat.att_g == 456.7
      assert rushing_stat.avg == 456.7
      assert rushing_stat.deleted_at == ~N[2011-05-18 15:01:01]
      assert rushing_stat.first_down_percentage == 456.7
      assert rushing_stat.first_downs == 456.7
      assert rushing_stat.forty_plus_downs == 43
      assert rushing_stat.fum == 43
      assert rushing_stat.lng == 43
      assert rushing_stat.lng_td == false
      assert rushing_stat.td == 43
      assert rushing_stat.twenty_plus_downs == 43
      assert rushing_stat.yds == 43
      assert rushing_stat.yds_g == 456.7
    end

    test "update_rushing_stat/2 with invalid data returns error changeset" do
      rushing_stat = rushing_stat_fixture()

      assert {:error, %Ecto.Changeset{}} =
               RushingStats.update_rushing_stat(rushing_stat, @invalid_attrs)

      assert rushing_stat.id == RushingStats.get_rushing_stat!(rushing_stat.id).id
    end

    test "nuke_rushing_stat/1 deletes the rushing_stat" do
      rushing_stat = rushing_stat_fixture()
      assert {:ok, %RushingStat{}} = RushingStats.nuke_rushing_stat(rushing_stat)
      assert_raise Ecto.NoResultsError, fn -> RushingStats.get_rushing_stat!(rushing_stat.id) end
    end

    test "soft_delete_rushing_stat/1 soft deletes the rushing_stat" do
      assert RushingStats.list_rushing_stats() == []
      rushing_stat_fixture(%{deleted_at: nil})
      assert length(RushingStats.list_rushing_stats()) == 1
    end

    test "change_rushing_stat/1 returns a rushing_stat changeset" do
      rushing_stat = rushing_stat_fixture()
      assert %Ecto.Changeset{} = RushingStats.change_rushing_stat(rushing_stat)
    end
  end
end
