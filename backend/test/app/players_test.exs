defmodule App.PlayersTest do
  use App.DataCase

  alias App.Players

  describe "players" do
    alias App.Players.Player

    @valid_attrs %{
      name: "some name",
      nfl_team: "some nfl_team",
      pos: "some pos"
    }
    @update_attrs %{
      name: "some updated name",
      nfl_team: "some updated nfl_team",
      pos: "some updated pos"
    }
    @invalid_attrs %{name: nil, nfl_team: nil, pos: nil}

    def player_fixture(attrs \\ %{}) do
      {:ok, player} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Players.create_player()

      player
    end

    test "list_players/0 returns all players" do
      player_fixture()
      assert length(Players.list_players()) == 1
    end

    test "get_player!/1 returns the player with given id" do
      player = player_fixture()
      assert Players.get_player!(player.id) == player
    end

    test "get_player_by_name/1 returns the player given a name" do
      player = player_fixture()
      assert Players.get_player_by_name(player.name) == player
      assert is_nil(Players.get_player_by_name("inexistent_name"))
    end

    test "create_player/1 with valid data creates a player" do
      assert {:ok, %Player{} = player} = Players.create_player(@valid_attrs)
      assert is_nil(player.deleted_at)
      assert player.name == "some name"
      assert player.nfl_team == "some nfl_team"
      assert player.pos == "some pos"
    end

    test "create_player/1 ensure unique name" do
      assert {:ok, %Player{} = player} = Players.create_player(@valid_attrs)
      assert player.name == "some name"
      assert {:error, err} = Players.create_player(@valid_attrs)
      assert err.errors |> hd |> elem(1) |> elem(0) == "has already been taken"
    end

    test "create_player/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Players.create_player(@invalid_attrs)
    end

    test "update_player/2 with valid data updates the player" do
      player = player_fixture()
      assert {:ok, %Player{} = player} = Players.update_player(player, @update_attrs)
      assert is_nil(player.deleted_at)
      assert player.name == "some updated name"
      assert player.nfl_team == "some updated nfl_team"
      assert player.pos == "some updated pos"
    end

    test "update_player/2 with invalid data returns error changeset" do
      player = player_fixture()
      assert {:error, %Ecto.Changeset{}} = Players.update_player(player, @invalid_attrs)
      assert player == Players.get_player!(player.id)
    end

    test "nuke_player/1 deletes the player" do
      player = player_fixture()
      assert {:ok, %Player{}} = Players.nuke_player(player)
      assert_raise Ecto.NoResultsError, fn -> Players.get_player!(player.id) end
    end

    test "soft_delete_player/1 soft deletes the player" do
      player = player_fixture()
      assert length(Players.list_players()) == 1
      assert {:ok, p} = Players.soft_delete_player(player)
      assert not is_nil(p.deleted_at)
      assert Players.list_players() == []
    end

    test "change_player/1 returns a player changeset" do
      player = player_fixture()
      assert %Ecto.Changeset{} = Players.change_player(player)
    end
  end
end
