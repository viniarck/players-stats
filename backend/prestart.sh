#! /usr/bin/env bash

echo "Waiting for the DB to start"
sleep 10;

echo "cd /app"
cd /app

echo "Trying to upgrade migrations to head"
mix local.rebar --force
mix deps.get
mix ecto.migrate
if [[ -z "$RUN_SEEDS_EXS" ]] ; then echo "skipping seeds.exs"; else mix run priv/repo/seeds.exs ; fi

mix phx.server
