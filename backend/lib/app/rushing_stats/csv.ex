defmodule App.RushingStats.CSV do
  @moduledoc """
  RushingStats CSV Parser.
  """
  NimbleCSV.define(RushingStatsParserCSV, separator: ",", escape: "\"")

  def dumps(rushing_stats) do
    values =
      rushing_stats
      |> Enum.map(fn r ->
        [
          r.att,
          r.att_g,
          r.avg,
          r.first_down_percentage,
          r.first_downs,
          r.forty_plus_downs,
          r.fum,
          r.lng,
          r.lng_td,
          r.td,
          r.twenty_plus_downs,
          r.yds,
          r.yds_g,
          case r.player != nil do
            true -> r.player.name
            false -> nil
          end,
          case r.player != nil do
            true -> r.player.nfl_team
            false -> nil
          end,
          case r.player != nil do
            true -> r.player.pos
            false -> nil
          end
        ]
      end)

    csv_header =
      ~w(att att_g avg first_down_percentage first_downs forty_plus_downs fum lng lng_td td twenty_plus_downs yds yds_g name nfl_team pos)

    [csv_header | values]
    |> RushingStatsParserCSV.dump_to_iodata()
    |> IO.iodata_to_binary()
  end
end
