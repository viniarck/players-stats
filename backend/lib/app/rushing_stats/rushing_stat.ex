defmodule App.RushingStats.RushingStat do
  @moduledoc """
  Rushing Statistics of a Player
  """

  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "rushing_stats" do
    field :att, :integer
    field :att_g, :float
    field :avg, :float
    field :deleted_at, :naive_datetime
    field :first_down_percentage, :float
    field :first_downs, :float
    field :forty_plus_downs, :integer
    field :fum, :integer
    field :lng, :integer
    field :lng_td, :boolean, default: false
    field :td, :integer
    field :twenty_plus_downs, :integer
    field :yds, :integer
    field :yds_g, :float
    belongs_to(:player, App.Players.Player)

    timestamps()
  end

  @doc false
  def changeset(rushing_stat, attrs) do
    rushing_stat
    |> cast(attrs, [
      :player_id,
      :att_g,
      :att,
      :yds,
      :avg,
      :yds_g,
      :td,
      :lng,
      :lng_td,
      :first_downs,
      :first_down_percentage,
      :twenty_plus_downs,
      :forty_plus_downs,
      :fum,
      :deleted_at
    ])
    |> validate_required([:lng_td])
  end
end
