defmodule App.RushingStats.RushingStatPaginationKW do
  @moduledoc """
  RushingStat Pagination Keywords Filters
  """

  use Ecto.Schema
  import Ecto.Changeset

  embedded_schema do
    field(:page, :integer)
    field(:per_page, :integer)
    field(:name, :string, default: nil)
    field(:order_by_columns, :string, default: "")
    field(:order_by_directions, :string, default: "")
    field(:as_csv, :boolean, default: false)
  end

  def build_filters(params \\ %{}) do
    params
    |> Map.keys()
    |> Enum.filter(fn key -> key in [:name] end)
    |> Enum.map(fn key -> {key, Map.get(params, key)} end)
    |> Enum.reduce(%{}, fn {key, val}, acc -> Map.put(acc, key, val) end)
  end

  def build_order_by(columns, sorting_directions)
      when is_bitstring(columns) and is_bitstring(sorting_directions) do
    sortable_columns =
      MapSet.new([
        "att",
        "att_g",
        "avg",
        "td",
        "first_down_percentage",
        "first_downs",
        "forty_plus_downs",
        "fum",
        "lng",
        "lng_td",
        "twenty_plus_downs",
        "yds",
        "yds_g",
        "name",
        "nfl_team",
        "pos"
      ])

    sorting_order_values = MapSet.new(["desc", "asc"])

    filtered_columns =
      columns
      |> String.split(",")
      |> Enum.filter(fn x -> x in sortable_columns end)
      |> Enum.map(fn x -> String.to_atom(x) end)

    filtered_sorting_directions =
      sorting_directions
      |> String.split(",")
      |> Enum.map(fn x ->
        case x in sorting_order_values do
          true -> x
          false -> "desc"
        end
      end)
      |> Enum.map(fn x -> String.to_atom(x) end)

    # Right pad :desc as default for the order by direction before zipping
    default_directions =
      0..(length(filtered_columns) - 1)
      |> Enum.map(fn _ -> :desc end)

    Enum.zip(filtered_sorting_directions ++ default_directions, filtered_columns)
  end

  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [:page, :per_page, :name, :order_by_columns, :order_by_directions, :as_csv])
    |> validate_required([:page, :per_page])
    |> validate_number(:page, greater_than_or_equal_to: 1)
    |> validate_number(:per_page, greater_than_or_equal_to: 1, less_than_or_equal_to: 1000)
  end
end
