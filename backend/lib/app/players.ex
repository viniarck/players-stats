defmodule App.Players do
  @moduledoc """
  The Players context.
  """

  import Ecto.Query, warn: false
  alias App.Repo

  alias App.Players.Player

  @doc """
  Returns the list of players.

  ## Examples

      iex> list_players()
      [%Player{}, ...]

  """
  def list_players do
    from(p in Player, where: is_nil(p.deleted_at), preload: [:rushing_stat]) |> Repo.all()
  end

  @doc """
  Gets a single player.

  Raises `Ecto.NoResultsError` if the Player does not exist.

  ## Examples

      iex> get_player!("123")
      %Player{}

      iex> get_player!("456")
      ** (Ecto.NoResultsError)

  """
  def get_player!(id), do: Repo.get!(Player, id)

  @doc """
  Gets a single player.

  ## Examples

      iex> get_player_by_name("existing_player_name")
      %Player{}

      iex> get_player_by_name("inexistent_name")
      nil

  """
  def get_player_by_name(name) do
    from(p in Player, where: p.name == ^name)
    |> Repo.one()
  end

  @doc """
  Creates a player.

  ## Examples

      iex> create_player(%{field: value})
      {:ok, %Player{}}

      iex> create_player(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_player(attrs \\ %{}) do
    %Player{}
    |> Player.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a player.

  ## Examples

      iex> update_player(player, %{field: new_value})
      {:ok, %Player{}}

      iex> update_player(player, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_player(%Player{} = player, attrs) do
    player
    |> Player.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Nukes a player (deletes from the DB).

  ## Examples

      iex> nuke_player(player)
      {:ok, %Player{}}

      iex> nuke_player(player)
      {:error, %Ecto.Changeset{}}

  """
  def nuke_player(%Player{} = player) do
    Repo.delete(player)
  end

  @doc """
  Soft deletes a player

  ## Examples

      iex> nuke_player(player)
      {:ok, %Player{}}

      iex> nuke_player(player)
      {:error, %Ecto.Changeset{}}

  """
  def soft_delete_player(%Player{} = player) do
    player
    |> change_player(%{deleted_at: NaiveDateTime.utc_now()})
    |> Repo.update()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking player changes.

  ## Examples

      iex> change_player(player)
      %Ecto.Changeset{data: %Player{}}

  """
  def change_player(%Player{} = player, attrs \\ %{}) do
    Player.changeset(player, attrs)
  end
end
