defmodule App.RushingStats do
  @moduledoc """
  The RushingStats context.
  """

  import Ecto.Query, warn: false
  import Ecto.Query.API, warn: false, only: [ilike: 2]
  alias App.Repo

  alias App.RushingStats.RushingStat
  alias App.Players.Player

  defp maybe_filter_by_name(query, name) when is_bitstring(name) do
    from(r in query,
      left_join: p in Player,
      on: r.player_id == p.id,
      where: is_nil(r.deleted_at) and is_nil(p.deleted_at) and ilike(p.name, ^"%#{name}%"),
      preload: [:player]
    )
  end

  defp maybe_filter_by_name(query, nil) do
    query
  end

  defp maybe_map_dynamic_order_by(tuple) when is_tuple(tuple) do
    player_attrs = MapSet.new([:name, :nfl_team, :pos])

    {order, attr} = {elem(tuple, 0), elem(tuple, 1)}

    case attr in player_attrs do
      true -> {order, dynamic([r, p], field(p, ^attr))}
      false -> {order, dynamic([r], field(r, ^attr))}
    end
  end

  defp maybe_order_by(query, tuples) when is_list(tuples) do
    from(r in query,
      left_join: p in Player,
      on: r.player_id == p.id,
      preload: [:player],
      order_by:
        ^(tuples
          |> Enum.map(fn tuple ->
            maybe_map_dynamic_order_by(tuple)
          end))
    )
  end

  defp maybe_order_by(query, nil) do
    query
  end

  defp maybe_limit_paginated_all(query, per_page, page)
       when is_number(per_page) and is_number(page) do
    limit = per_page
    offset = (page - 1) * limit

    {from(r in query, limit: ^limit, offset: ^offset) |> Repo.all(),
     query |> Repo.aggregate(:count, :id)}
  end

  defp maybe_limit_paginated_all(query, _limit, _offset) do
    query |> Repo.all()
  end

  @doc """
  Returns the list of rushing_stats.

  opts \\ [filters: %{}, order_by: [desc: :td], per_page: 50, page: 1]

  ## Examples

      iex> list_rushing_stats(%{})
      [%RushingStat{}, ...]

  """
  def list_rushing_stats(opts \\ []) do
    defaults = [filters: %{}, order_by: [desc: :td]]
    opts = Keyword.merge(defaults, opts)

    from(r in RushingStat,
      left_join: p in Player,
      on: r.player_id == p.id,
      where: is_nil(r.deleted_at) and is_nil(p.deleted_at),
      preload: [:player]
    )
    |> maybe_filter_by_name(opts[:filters][:name])
    |> maybe_order_by(opts[:order_by])
    |> maybe_limit_paginated_all(opts[:per_page], opts[:page])
  end

  @doc """
  Gets a single rushing_stat.

  Raises `Ecto.NoResultsError` if the Rushing stat does not exist.

  ## Examples

      iex> get_rushing_stat!("123")
      %RushingStat{}

      iex> get_rushing_stat!("456")
      ** (Ecto.NoResultsError)

  """
  def get_rushing_stat!(id),
    do:
      from(r in RushingStat, where: r.id == ^id and is_nil(r.deleted_at), preload: [:player])
      |> Repo.one!()

  @doc """
  Creates a rushing_stat.

  ## Examples

      iex> create_rushing_stat(%{field: value})
      {:ok, %RushingStat{}}

      iex> create_rushing_stat(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_rushing_stat(attrs \\ %{}) do
    %RushingStat{}
    |> RushingStat.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a rushing_stat.

  ## Examples

      iex> update_rushing_stat(rushing_stat, %{field: new_value})
      {:ok, %RushingStat{}}

      iex> update_rushing_stat(rushing_stat, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_rushing_stat(%RushingStat{} = rushing_stat, attrs) do
    rushing_stat
    |> RushingStat.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a rushing_stat.

  ## Examples

      iex> nuke_rushing_stat(rushing_stat)
      {:ok, %RushingStat{}}

      iex> nuke_rushing_stat(rushing_stat)
      {:error, %Ecto.Changeset{}}

  """
  def nuke_rushing_stat(%RushingStat{} = rushing_stat) do
    Repo.delete(rushing_stat)
  end

  @doc """
  Soft deletes a rushing_stat.

  ## Examples

      iex> nuke_rushing_stat(rushing_stat)
      {:ok, %RushingStat{}}

      iex> nuke_rushing_stat(rushing_stat)
      {:error, %Ecto.Changeset{}}

  """
  def soft_delete_rushing_stat(%RushingStat{} = rushing_stat) do
    rushing_stat
    |> change_rushing_stat(%{deleted_at: NaiveDateTime.utc_now()})
    |> Repo.update()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking rushing_stat changes.

  ## Examples

      iex> change_rushing_stat(rushing_stat)
      %Ecto.Changeset{data: %RushingStat{}}

  """
  def change_rushing_stat(%RushingStat{} = rushing_stat, attrs \\ %{}) do
    RushingStat.changeset(rushing_stat, attrs)
  end
end
