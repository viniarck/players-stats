defmodule App.Players.Player do
  @moduledoc """
  Player Schema.
  """

  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "players" do
    field :deleted_at, :naive_datetime
    field :name, :string
    field :nfl_team, :string
    field :pos, :string
    has_one(:rushing_stat, App.RushingStats.RushingStat)

    timestamps()
  end

  @doc false
  def changeset(player, attrs) do
    player
    |> cast(attrs, [:name, :nfl_team, :pos, :deleted_at])
    |> validate_required([:name])
    |> validate_length(:name, min: 1, max: 256)
    |> unique_constraint(:name)
  end
end
