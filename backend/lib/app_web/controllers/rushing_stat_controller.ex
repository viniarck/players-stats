defmodule AppWeb.RushingStatController do
  use AppWeb, :controller

  alias App.RushingStats
  alias App.RushingStats.RushingStat
  alias App.RushingStats.RushingStatPaginationKW

  action_fallback AppWeb.FallbackController

  defp index_render(conn, changeset, _valid = true) do
    page = changeset.changes.page
    per_page = changeset.changes.per_page
    order_by_columns = Map.get(changeset.changes, :order_by_columns, "")
    order_by_directions = Map.get(changeset.changes, :order_by_directions, "")
    as_csv = Map.get(changeset.changes, :as_csv, false)

    {rushing_stats, count} =
      RushingStats.list_rushing_stats(
        page: page,
        per_page: per_page,
        filters: RushingStatPaginationKW.build_filters(changeset.changes),
        order_by: RushingStatPaginationKW.build_order_by(order_by_columns, order_by_directions)
      )

    render(conn, "index.json", %{
      rushing_stats: rushing_stats,
      count: count,
      page: page,
      per_page: per_page,
      as_csv: as_csv
    })
  end

  defp index_render(conn, changeset, _valid = false) do
    conn
    |> put_status(400)
    |> render("error_all.json", %{
      errors:
        changeset.errors
        |> Enum.map(fn x -> %{key: elem(x, 0), reason: elem(x, 1) |> elem(0)} end)
    })
  end

  def index(conn, params) do
    changeset =
      RushingStatPaginationKW.changeset(%{
        page: Map.get(params, "page", 1),
        per_page: Map.get(params, "per_page", 50),
        name: Map.get(params, "name"),
        order_by_columns: Map.get(params, "order_by_columns", ""),
        order_by_directions: Map.get(params, "order_by_directions", ""),
        as_csv: Map.get(params, "as_csv", false)
      })

    index_render(conn, changeset, changeset.valid?)
  end

  def create(conn, %{"rushing_stat" => rushing_stat_params}) do
    case RushingStats.create_rushing_stat(rushing_stat_params) do
      {:ok, %RushingStat{} = rushing_stat} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.rushing_stat_path(conn, :show, rushing_stat))
        |> render("show.json", rushing_stat: rushing_stat)

      _ ->
        put_status(conn, :unprocessable_entity)
        |> render("error_item.json", id: rushing_stat_params |> Map.get(:id), error: "error")
    end
  end

  def show(conn, %{"id" => id}) do
    rushing_stat = RushingStats.get_rushing_stat!(id)
    render(conn, "show.json", rushing_stat: rushing_stat)
  end

  def update(conn, %{"id" => id, "rushing_stat" => rushing_stat_params}) do
    rushing_stat = RushingStats.get_rushing_stat!(id)

    case RushingStats.update_rushing_stat(rushing_stat, rushing_stat_params) do
      {:ok, %RushingStat{} = rushing_stat} ->
        render(conn, "show.json", rushing_stat: rushing_stat)

      _ ->
        put_status(conn, :unprocessable_entity)
        |> render("error_item.json", id: id, error: "error")
    end
  end

  def delete(conn, %{"id" => id}) do
    rushing_stat = RushingStats.get_rushing_stat!(id)

    with {:ok, %RushingStat{}} <- RushingStats.soft_delete_rushing_stat(rushing_stat) do
      send_resp(conn, :no_content, "")
    end
  end
end
