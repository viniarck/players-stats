defmodule AppWeb.RushingStatView do
  use AppWeb, :view
  alias AppWeb.RushingStatView
  alias App.RushingStats.CSV

  def render("index.json", %{
        rushing_stats: rushing_stats,
        count: count,
        page: page,
        per_page: per_page,
        as_csv: _as_csv = false
      }) do
    %{
      data: render_many(rushing_stats, RushingStatView, "rushing_stat.json"),
      pagination: %{total: count, page: page, per_page: per_page}
    }
  end

  def render("index.json", %{
        rushing_stats: rushing_stats,
        count: count,
        page: page,
        per_page: per_page,
        as_csv: _as_csv = true
      }) do
    %{
      data: render_one(rushing_stats, RushingStatView, "rushing_stat_csv.json"),
      pagination: %{total: count, page: page, per_page: per_page}
    }
  end

  def render("show.json", %{rushing_stat: rushing_stat}) do
    %{data: render_one(rushing_stat, RushingStatView, "rushing_stat.json")}
  end

  def render("rushing_stat_csv.json", %{rushing_stat: r}) do
    CSV.dumps(r)
  end

  def render("rushing_stat.json", %{rushing_stat: r}) do
    %{
      id: r.id,
      att: r.att,
      att_g: r.att_g,
      avg: r.avg,
      first_down_percentage: r.first_down_percentage,
      first_downs: r.first_downs,
      forty_plus_downs: r.forty_plus_downs,
      fum: r.fum,
      lng: r.lng,
      lng_td: r.lng_td,
      td: r.td,
      twenty_plus_downs: r.twenty_plus_downs,
      yds: r.yds,
      yds_g: r.yds_g,
      player: render_player(r.player)
    }
  end

  def render("error_item.json", %{id: id, error: err}) do
    %{errors: [%{id: id, error: err}]}
  end

  def render("error_all.json", %{errors: errors}) do
    %{errors: errors}
  end

  defp render_player(player = %App.Players.Player{}) do
    %{name: player.name, pos: player.pos, nfl_team: player.nfl_team}
  end

  defp render_player(_) do
    nil
  end
end
