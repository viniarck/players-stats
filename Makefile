build:
	COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose build

test:
	docker exec backend bash -c "mix test --trace"

run-seeds:
	docker exec backend bash -c "mix run priv/repo/seeds.exs"

mix-deps:
	docker exec backend bash -c "mix deps.get"

credo:
	cd backend && mix credo

test-local:
	cd backend && mix test --trace

compose-up:
	docker-compose up -d

compose-down:
	docker-compose down

compose-down-v:
	docker-compose down -v

.PHONY: all test clean
