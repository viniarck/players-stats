
## `players-stats` technical overview

Based on the [requirements](./requirements.md), I think that that a full-stack web framework with a relational database storage would fit well for this app, while being a very battle tested and flexible approach to go with.

## `players-stats` web stack

For the web stack, I chose to implement the back-end in Elixir with Phoenix, which exposes the underlying players stats data models over a JSON API, that's consumed by a front-end that was implemented in Vue.js. I think that this stack would be a pretty safe and scalable approach, one in particular that I'm familiar with as well. But, I think that this web app be neat to be implemented with [LiveView](https://github.com/phoenixframework/phoenix_live_view), would even be faster to implement, iterate and maintain. Nevertheless, a decoupled front-end also has their pros (and some cons). This diagram below illustrates a high level view of the core parts of this app:

![high_level](./high_level.png)

## DB modeling

For the data modeling, I decided to go with some normalization, but not completely normalized entities based what what it's known in the short/medium term. I think de-coupling the players from their rushing stats in two tables, would be easier to extend in the future to potentially add additional statistics features like receiving, passing and so on. This image shows the tables and their relationship:

![db_schemas](./db_schemas.png)

### Indexes

Based on what it's known upfront, some indexes for the player's name, position, team and foreign keys have been created, since users searching by a players' name is one of the features that are in the hot path of the application. Also a index for the player name and the player_id fk have been created to ensure uniqueness based on the requirements.

## Back-end runtime

The back-end is powered by Elixir, and initially it would be shipped in an container without any Erlang clustering, and potentially horizontal scaling with a load balancer could be used spinning up new instances. Similarly we could use a elastic relational DB accordingly or start with a more affordable upfront depending on the expected traffic.

## Caching

Upfront there's no caching implemented, but I think ETS or Redis could come in handy depending on how this application load traffic evolves and the data consistency requirements that are expected to be displayed

## CI/CD

Initially CI was setup on [GitLab](../.gitlab-ci.yml), which runs unit and integration tests for the backend in Elixir. You can check the result of an execution [here](https://gitlab.com/viniarck/players-stats/-/jobs/1398325192) I think additional linting stages and front-end tests would be handy to have in a future iteration
