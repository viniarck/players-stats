Additional questions that I asked and Maiwand has clarified:


The short answer to most questions is: the end goal is a user-facing application trying to provide the best user experience while being constrained by development time guidelines of a "few hours".  For example as an end-user I rarely know the exact case-sensitive, correctly spelled name I am searching for therefore partial case-insensitive search makes sense.
From the engineering side, the systems are rarely fully specified down to exact requests per second.  Please feel free to make assumptions based on your experience and implement based on those.  Please feel free to communicate the assumptions and design choices.

1. Should the sorting feature only sort a single attribute that will be specified by the user or is it supposed to enable sorting by multiple columns at the same time?
   - multiple attributes for best user experience

2. When sorting by _Longest Rush_ would it be OK to only consider the numerical part ignoring if the longest rush was a touchdown or not?
   - requires that you sort by numerical value and use the touchdown indicator as a tiebreaker

3. Should the user be able to set whether the sorting is supposed to be ascending or descending? Let me know if there are preferences for the default sorting criteria in case it's not specified.
   - both ascending and descending would be preferred end-user functionality
Regarding requirement b) -> The user should be able to filter by the player's name:

4. Should the filtering capability be an exact match or is it also desirable to have substring case sensitive match, even though the performance might be slightly slower?
   - case insensitive partial matches
Regarding requirement c) -> The system should be able to potentially support larger sets of data on the order of 10k records:

5. Should this web app ensure uniqueness for a player's name and their rushing statistics? Let me know if any other uniqueness guarantees are expected, including other business validation rules that might be desirable to have. What's the expected behavior if a same player name appears twice when inserting data in the database?
   - assume player names must be unique.  For the rest please make an assumption based on the sample data for this implementation.

6. In order to support up to 10k records, I'd like to propose implementing pagination, let me know if you have any preference or if I could proceed with a standard implementation using a total counter with page number and offset, each parameter the user would be able to set accordingly.
   - pagination sounds like a great feature.  Please implement using your best judgement and expertise.
Additional general questions to better understanding holistically how this web app will be used:

7. Should this app be prepared to also potentially support other similar statistics like passing or receiving of a given player in the short term?
   - Unspecified.  Assume the app may be refactored to extend functionality in any way that is impossible to predict.

8. Would this web app might be accessed by other platforms that might need a JSON API? and what's the initial expected traffic load in terms of requests/secs that should be expected during a first iteration of this web app, assuming that it would be eventually deployed in a cloud environment?
   - Assume no other platforms.

9. Would it be safe to assume that user authentication won't be needed in the short/medium term?
   - Yes, no authentication required.
